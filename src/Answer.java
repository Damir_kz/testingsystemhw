import java.util.List;

public class Answer {
    private final String textOfAnswer;
    private boolean isTrueAnswer;
    private boolean isSelected;
    private double priceOfAnswer;
    private final int id;

    public static void priceDivider(List<Answer> list, double settablePrice){
        for(Answer x: list){
            if (x.isTrueAnswer()) {
                x.setPriceOfAnswer(settablePrice);
            }
        }
    }
    public Answer(String textOfAnswer, int id ) {
        this.textOfAnswer=textOfAnswer;
        this.id = id;
    }

    public String getTextOfAnswer() {
        return textOfAnswer;
    }

    public boolean isTrueAnswer() {
        return isTrueAnswer;
    }

    public void setTrueAnswer(boolean trueAnswer) {
        isTrueAnswer=trueAnswer;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void makeSelected() {
        isSelected=true;
    }

    public void makeNotSelected(){
        isSelected = false;
    }

    public double getPriceOfAnswer() {
        return priceOfAnswer;
    }

    public void setPriceOfAnswer(double priceOfAnswer) {
        this.priceOfAnswer=priceOfAnswer;
    }

    public int getId() {
        return id;
    }
}
