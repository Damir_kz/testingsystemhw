import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private final List<Question> listOfQuestion = new ArrayList<>();
    static final Scanner in = new Scanner(System.in);
    private double finalScore;
    private long timeOfBegin;
    private double maxVal = 0;

    public static void main(String[] args) {
        Main main=new Main();
        main.startMenu();
    }

    public void startMenu() {
        System.out.println("1) Добавить вопрос" + "\n" + "2) Начать тестирование" + "\n" + "3) Выйти ");
        String userChoice=Stringer.enterString();
        switch (userChoice) {
            case "1" -> addQuestionInfo(new Question());
            case "2" -> goTesting();
            case "3" -> System.exit(0);
            default -> {
                System.out.println("Вы ввели неправильное значение");
                startMenu();
            }
        }
    }

    public void addQuestionInfo(Question question) {
        System.out.println("Введите вопрос: ");
        question.setTextOfQuestion(Stringer.enterString());
        System.out.println("Количество ответов: ");

        question.setQuantityOfAnswer(in.nextInt());
        while (question.getQuantityOfAnswer() <= 0) {
            System.out.println("Введите положительное значение");
            question.setQuantityOfAnswer(in.nextInt());
        }

        System.out.println("Введите ответы: ");
        for (int i=0; i < question.getQuantityOfAnswer(); i++) {
            System.out.print((i + 1) + ") ");
            question.getListOfAnswer().add(new Answer(Stringer.enterString(), i + 1));
        }

        System.out.println("Количество правильных ответов: ");
        question.setQuantityOfTrueAnswers(in.nextInt());

        while ( question.getQuantityOfTrueAnswers() > question.getQuantityOfAnswer() ||
        question.getQuantityOfTrueAnswers()<=0){
            System.out.println("Введите корректное значение");
            question.setQuantityOfTrueAnswers(in.nextInt());
        }

        System.out.println("Введите номера правильных ответов: ");

        for (int i=0; i < question.getQuantityOfTrueAnswers(); i++) {
            int userSelectedId = in.nextInt() - 1;
            while (userSelectedId > question.getListOfAnswer().size() || userSelectedId < 0){
                System.out.println("Введите корректное значение");
                userSelectedId = in.nextInt() - 1;
            }
            question.getListOfAnswer().get(userSelectedId).setTrueAnswer(true);
        }
        System.out.println("Стоимость вопроса: ");
        question.setPriceOfQuestion(in.nextInt());
        while (question.getPriceOfQuestion()<=0){
            System.out.println("Введите положительное значение");
            question.setPriceOfQuestion(in.nextInt());
        }
        Answer.priceDivider(question.getListOfAnswer(),
                question.getPriceOfQuestion() / question.getQuantityOfTrueAnswers());

        listOfQuestion.add(question);
        startMenu();
    }


    public void goTesting() {
        maxVal=0;
        finalScore=0;
        Question.clearQuestions(listOfQuestion);

        System.out.println("Укажи количество будущих вопросов");
        int countOfFutureQuestions=in.nextInt();
        while (countOfFutureQuestions > listOfQuestion.size()) {
            System.out.println("Введенное значение больше допустимого" + "\n" + "Введите заново: ");
            countOfFutureQuestions=in.nextInt(); // я вас не слышу !!11!
        }

        for (int i=0; i < countOfFutureQuestions; i++) {
            Question randomQuestion=getRandomQuestions();
            maxVal+=randomQuestion.getPriceOfQuestion();
            System.out.println(randomQuestion.toString());

            timeOfBegin=System.currentTimeMillis();

            System.out.println("Введите ответ: ");
            String[] userPickedAnswersId=Stringer.enterString().split(" ");
            for (String s : userPickedAnswersId) {
                int target=Integer.parseInt(s);
                Answer userPickedAnswer=randomQuestion.getListOfAnswer().get(target - 1);
                userPickedAnswer.makeSelected();
                if (userPickedAnswer.isTrueAnswer()) {
                    randomQuestion.setQuantityOfTrueAnswers(randomQuestion.getQuantityOfTrueAnswers() - 1);
                    finalScore+=userPickedAnswer.getPriceOfAnswer();
                }
            }
            if (randomQuestion.getQuantityOfTrueAnswers() == 0) {
                randomQuestion.makeSolvedRight();
            }
        }
        showUserResults();
    }

    public Question getRandomQuestions() {
        int randomIndex=(int) (Math.random() * listOfQuestion.size());
        while (listOfQuestion.get(randomIndex).isAlreadyPickedByRand()) {
            randomIndex=(int) (Math.random() * listOfQuestion.size());
        }
        listOfQuestion.get(randomIndex).makeAlreadyPickedByRand();
        return listOfQuestion.get(randomIndex);
    }


    public void showUserResults() {
        System.out.println
                ("Вы набрали: " + finalScore + " из " + maxVal + "\n" +
                        "Это заняло у вас: " + (System.currentTimeMillis() - timeOfBegin) / 1000 + " sec");

        if (finalScore != maxVal) {
            Question.showNotSolvedQuestions(listOfQuestion);
        }
        userNextMove();
    }

    public void userNextMove() {
        System.out.println("Что желаете ? ");
        System.out.println("1) Пройти еще один тест" + "\n" + "2) Вернуться в главное меню" + "\n" + "3) Выйти");
        String move=Stringer.enterString();
        switch (move) {
            case "1":
                goTesting();
            case "2":
                startMenu();
            case "3":
                System.exit(0);
            default:
                System.out.println("Вы ввели некорректное значение");
                userNextMove();
        }
    }
}
