import java.util.ArrayList;
import java.util.List;

public class Question {
    private String textOfQuestion;
    private int quantityOfAnswer;
    private int quantityOfTrueAnswers;
    private boolean isAlreadyPickedByRand;
    private double priceOfQuestion;
    private boolean isSolvedRight;
    private final List<Answer> listOfAnswer = new ArrayList<>(quantityOfAnswer);


    @Override
    public String toString() {
        return (this.getTextOfQuestion() + "\n" + showVariants(listOfAnswer));
    }

    public static void showNotSolvedQuestions(List<Question> list){
        System.out.println("Ошибки в вопросах: ");
        for(Question x: list){
            if( !x.isSolvedRight ){
                System.out.println(x.getTextOfQuestion());
                System.out.println("Ожидалось: ");
                for (int i = 0; i<x.getListOfAnswer().size();i++){
                    Answer item = x.getListOfAnswer().get(i);
                    if(item.isTrueAnswer()){
                        System.out.println(item.getId()+") " + item.getTextOfAnswer());
                    }
                }
                System.out.println("Получено: ");
                for (int i = 0; i<x.getListOfAnswer().size();i++){
                    Answer item = x.getListOfAnswer().get(i);
                    if(item.isSelected()){
                        System.out.println(item.getId()+") " + item.getTextOfAnswer());
                    }
                }
            }
        }
    }

    public static void clearQuestions(List<Question> list){
        for (Question x: list){
            x.makeNotPickedByRand();
            for(int i = 0; i< x.getListOfAnswer().size();i++){
                x.getListOfAnswer().get(i).makeNotSelected();
            }
        }
    }

    public void setQuantityOfAnswer(int quantityOfAnswer) {
        this.quantityOfAnswer=quantityOfAnswer;
    }

    public String showVariants(List<Answer> listOfAnswer) {
        String s = "";
        for (int i = 0;i<listOfAnswer.size();i++) {
            s = s+(i+1) + ") " +  listOfAnswer.get(i).getTextOfAnswer() + "\n";
        }
        if(s.equals("")) return "Вариантов ответа нет )))";
        return s;
    }

    public int getQuantityOfAnswer() {
        return quantityOfAnswer;
    }

    public int getQuantityOfTrueAnswers() {
        return quantityOfTrueAnswers;
    }

    public void setQuantityOfTrueAnswers(int quantityOfTrueAnswers) {
        this.quantityOfTrueAnswers=quantityOfTrueAnswers;
    }

    public double getPriceOfQuestion() {
        return priceOfQuestion;
    }

    public void setPriceOfQuestion(int priceOfQuestion) {
        this.priceOfQuestion=priceOfQuestion;

    }
    public void makeSolvedRight() {
        isSolvedRight=true;
    }
    public String getTextOfQuestion() {
        return textOfQuestion;
    }
    public void setTextOfQuestion(String textOfQuestion) {
        this.textOfQuestion=textOfQuestion;
    }
    public List<Answer> getListOfAnswer() {
        return listOfAnswer;
    }
    public boolean isAlreadyPickedByRand() {
        return isAlreadyPickedByRand;
    }
    public void makeAlreadyPickedByRand() {
        isAlreadyPickedByRand=true;
    }
    public void makeNotPickedByRand(){
        isAlreadyPickedByRand=false;
    }

}
