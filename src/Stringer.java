import java.util.Scanner;

public class Stringer {
    private static final Scanner in = new Scanner(System.in);
    public static String enterString(){
        String s = in.nextLine();
        if(s.equals("")){
            return in.nextLine();
        }else{
            return s;
        }
    }
}
